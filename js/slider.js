const prev = document.querySelector('#prev')
const next = document.querySelector('#next')
const sliderContent = document.querySelectorAll('.slider-content .slide__img')
const sliderWrapper = document.querySelector('.slides-wrapper')
const slider = document.querySelector('.slides')


const updateContent = (dataValue, parent, needToChange, className) => {
    parent.forEach(cont => (dataValue === cont.dataset.slide) ?
        cont.closest(needToChange).classList.add(className) :
        cont.closest(needToChange).classList.remove(className))
}

const initSlider = (parent, amount) => {
    let start = [...parent.children].slice(0, amount)
    let end = [...parent.children].slice(parent.children.length - amount).reverse()
    for (let i = 0; i < amount; i++) {
        parent.append(start[i].cloneNode(true))
        parent.prepend(end[i].cloneNode(true))
    }
}

const sliderCaruosel = (numVisibleImgs, parent, imgsClass) => {
    const moveSlide = (anim, slider, elems, className, value, numSlides) => {
        if (!can) return
        slider.style.transition = anim
        const index = (numSlides > 0) ?
            (firstActive([...elems], className) + numSlides) :
            (findActiveRev([...elems], className) + numSlides)
        elems.forEach(img => (elems[index].dataset.slide === img.dataset.slide) ?
            img.classList.add(className) :
            img.classList.remove(className))
        pos += value
        slider.style.transform = `translateX(${pos}px)`
        can = false
        return index
    }

    initSlider(parent, numVisibleImgs)
    const slides = document.querySelectorAll(imgsClass)
    const sliderImglen = slides.length - numVisibleImgs * 2
    const slideWidth = 90
    const end = slides.length * slideWidth - numVisibleImgs * slideWidth
    slider.style.transform = `translateX(${-slideWidth * numVisibleImgs}px)`
    let pos = parseInt((slider.style.transform).match(/translate.*\((.+)\)/)[1])
    let can = true


    const move = (width, amount) => {
        const index = moveSlide('ease-in-out .3s', slider, slides, 'active--slide', width, amount)
        if (index) {
            updateContent(slides[index].dataset.slide, sliderContent, '.slider-content__item', 'visible--slide')
        }
    }

    next.addEventListener('click', () => move(-slideWidth, 1))
    prev.addEventListener('click', () => move(slideWidth, -1))

    sliderWrapper.addEventListener('transitionend', () => {
        can = true
        const currPos = Math.abs(pos)
        if (currPos >= end) {
            moveSlide('ease-in-out 0s', slider, slides, 'active--slide', sliderImglen * slideWidth, sliderImglen)
        } else if (currPos <= 0) {
            moveSlide('ease-in-out 0s', slider, slides, 'active--slide', -sliderImglen * slideWidth, -sliderImglen)
        }
        can = true
    })

    sliderWrapper.addEventListener('click', e => {
        if (e.target.classList.contains('slide__img')) {
            const curr = e.target.dataset.slide
            slides.forEach(img => (curr === img.dataset.slide) ? img.classList.add('active--slide') : img.classList.remove('active--slide'))
            updateContent(curr, sliderContent, '.slider-content__item', 'visible--slide')
        }
    })
}

sliderCaruosel(4, slider, '.slides .slide__img')